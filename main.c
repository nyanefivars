/*
 * Protected by a GNU AFFERO GPLv3 
 * Copyright (C) 2021
 * Sylvain Bertrand <sylvain.bertrand@legeek.net>
 */
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <endian.h>
#include <string.h>

#define loop for(;;)
#define strtou64 strtoul
#define u16 uint16_t
#define u32 uint32_t
#define u8 uint8_t
#define u64 uint64_t

#define EFI_VARIABLE_NON_VOLATILE	0x00000001
#define EFI_VARIABLE_BOOTSERVICE_ACCESS	0x00000002
#define EFI_VARIABLE_RUNTIME_ACCESS	0x00000004

#define LOAD_OPTION_ACTIVE		0x00000001

#define PARTITION_UUID		1
#define PARTITION_NUMBER	2
#define PARTITION_START_LBA	3
#define PARTITION_LBAS_N	4
#define DESCRIPTION		5
#define LOADER_FILE_PATH	6
#define LOADER_ARGS		7

struct guid_t {
	u32 blk_0;
	u16 blk_1;
	u16 blk_2;
	u16 blk_3;
	u8 blk_4[6];
};
/* brain damaged mixed-endian guid */
static void guid_write(void *dest, struct guid_t *src)
{
	u8 *d;
	u32 *p32;
	u16 *p16;

	d = (u8*)dest;

	p32 = (u32*)d;
	*p32 = htole32(src->blk_0); /* little endian */
	d += 4;
	p16 = (u16*)d;
	*p16 = htole16(src->blk_1); /* little endian */
	d += 2;
	p16 = (u16*)d;
	*p16 = htole16(src->blk_2); /* little endian */
	d += 2;
	p16 = (u16*)d;
	*p16 = htobe16(src->blk_3); /* big endian */
	d += 2;
	d[0] = src->blk_4[0];
	d[1] = src->blk_4[1];
	d[2] = src->blk_4[2];
	d[3] = src->blk_4[3];
	d[4] = src->blk_4[4];
	d[5] = src->blk_4[5];
}
/* best effort */
static void guid_read_from_args(struct guid_t *guid, void *arg)
{
	u8 *blk_start;
	u8 *blk_end;
	u8 save;
	u8 *end;

	memset(guid, 0, sizeof(*guid));
	end = arg + strlen(arg);

	blk_start = arg;
	blk_end =  strchr(blk_start, '-');
	if (blk_end == 0)
		return;
	*blk_end = 0;
	guid->blk_0 = (u32)strtou64(blk_start, 0, 16);
	blk_start = blk_end + 1;
	if (blk_start >= end)
		return;
	blk_end = strchr(blk_start, '-');
	if (blk_end == 0)
		return;
	*blk_end = 0;
	guid->blk_1 = (u16)strtou64(blk_start, 0, 16);
	blk_start = blk_end + 1;
	if (blk_start >= end)
		return;
	blk_end = strchr(blk_start, '-');
	if (blk_end == 0)
		return;
	*blk_end = 0;
	guid->blk_2 = (u16)strtou64(blk_start, 0, 16);
	blk_start = blk_end + 1;
	if (blk_start >= end)
		return;
	blk_end = strchr(blk_start, '-');
	if (blk_end == 0)
		return;
	*blk_end = 0;
	guid->blk_3 = (u16)strtou64(blk_start, 0, 16);
	/* blk_4 */
	blk_start = blk_end + 1;
	if (blk_start >= end)
		return;
	blk_end = blk_start + 12; /* 6 bytes */
	if (blk_end > end)
		return;
	save = blk_start[2];
	blk_start[2] = 0;
	guid->blk_4[0] = (u8)strtou64(&blk_start[0], 0, 16);
	blk_start[2] = save;

	save = blk_start[4];
	blk_start[4] = 0;
	guid->blk_4[1] = (u8)strtou64(&blk_start[2], 0, 16);
	blk_start[4] = save;

	save = blk_start[6];
	blk_start[6] = 0;
	guid->blk_4[2] = (u8)strtou64(&blk_start[4], 0, 16);
	blk_start[6] = save;

	save = blk_start[8];
	blk_start[8] = 0;
	guid->blk_4[3] = (u8)strtou64(&blk_start[6], 0, 16);
	blk_start[8] = save;

	save = blk_start[10];
	blk_start[10] = 0;
	guid->blk_4[4] = (u8)strtou64(&blk_start[8], 0, 16);
	blk_start[10] = save;

	guid->blk_4[5] = (u8)strtou64(&blk_start[10], 0, 16);
}

static void usage(void)
{
	printf("\
Generate a UEFI load option variable on the standard output intended for linux\n\
efivarfs: /sys/firmware/efi/efivars/BootXXXX-8be4df61-93ca-11d2-aa0d-00e098032b8c\n\
Use a command like \"nyangpt\" in order to get the partition UUID (not the\n\
filesystem UUID), number (starting from 1), starting LBA, size in cound of LBAs.\n\
Don't forget to create/update\n\
/sys/firmware/efi/efivars/BootOrder-8be4df61-93ca-11d2-aa0d-00e098032b8c.\n\
\n\
nyanefivars partition_uuid partition_number partition_starting_lba partition_size_lbas description_ascii loader_file_path_ascii [loader_arguments_ascii]\n"
	);
	exit(1);
}
int main(int args_n, u8 **args)
{
	u8 *buf;
	ssize_t buf_bytes_n;

	u32 *p32;
	u8 *p8;
	u8 *p8_src;
	u8 *p8_dst;
	u16 *p16;
	u64 *p64;

	u32 UEFIVarAttrs;
	u32 LoadOptionAttrs;
	u16 FilePathListLength;
	u16 desc_bytes_n;

	u16 HardDriveMediaDevicePath_Length;
	struct guid_t partition_guid;
	u32 partition_number;
	u64 partition_start_lba;
	u64 partition_lbas_n;
	u16 FilePathMediaDevicePath_Length;

	ssize_t written_bytes_n;

	if (args_n != 7 && args_n != 8)
		usage();
	buf = 0;
	buf_bytes_n = 0;
	/* linux specific, the variable attrs goes as a header */
	UEFIVarAttrs = EFI_VARIABLE_NON_VOLATILE
		| EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS;
	buf_bytes_n = sizeof(UEFIVarAttrs);
	buf = realloc(buf, (size_t)buf_bytes_n);
	p32 = (u32*)buf;
	*p32 = htole32(UEFIVarAttrs);

	LoadOptionAttrs = LOAD_OPTION_ACTIVE;
	buf_bytes_n += sizeof(LoadOptionAttrs);
	buf = realloc(buf, (size_t)buf_bytes_n);
	p32 = (u32*)(buf + sizeof(UEFIVarAttrs));
	*p32 = htole32(LoadOptionAttrs);
	/* we don't know FilePathListLength yet */
	buf_bytes_n += sizeof(FilePathListLength);
	buf = realloc(buf, (size_t)buf_bytes_n);
	/* description */
	desc_bytes_n = (strlen(args[DESCRIPTION]) + 1) * 2; /* account for the 0 terminating ucs2 char */
	buf_bytes_n += desc_bytes_n;
	buf = realloc(buf, (size_t)buf_bytes_n);
	p8_src = args[DESCRIPTION];
	p8_dst = buf	+ sizeof(UEFIVarAttrs)
			+ sizeof(LoadOptionAttrs)
			+ sizeof(FilePathListLength);
	loop { /* ascii to ucs2 */
		p8_dst[0] = p8_src[0];
		p8_dst[1] = 0;
		if (p8_src[0] == 0)
			break;
		++p8_src;
		p8_dst += 2;
	}
	/* Hard Drive Media Device Path -- START */
	HardDriveMediaDevicePath_Length = 42;
	buf_bytes_n += HardDriveMediaDevicePath_Length;
	buf = realloc(buf, (size_t)buf_bytes_n);
	p8 = buf	+ sizeof(UEFIVarAttrs)
			+ sizeof(LoadOptionAttrs)
			+ sizeof(FilePathListLength)
			+ desc_bytes_n;
	p8[0] = 0x04; /* Media Device Path */
	p8[1] = 0x01; /* Hard Drive */
	p8[2] = (u8)HardDriveMediaDevicePath_Length; /* length */
	p8[3] = 0x00;
	partition_number = (u32)strtou64(args[PARTITION_NUMBER], 0, 10);
	p32 = (u32*)(p8 + 4);
	*p32 = htole32(partition_number);
	partition_start_lba = strtou64(args[PARTITION_START_LBA], 0, 10);
	p64 = (u64*)(p8 + 8);
	*p64 = htole64(partition_start_lba);
	partition_lbas_n = strtou64(args[PARTITION_LBAS_N], 0, 10);
	p64 = (u64*)(p8 + 16);
	*p64 = htole64(partition_lbas_n);
	guid_read_from_args(&partition_guid, args[PARTITION_UUID]);
	guid_write(p8 + 24, &partition_guid);
	p8[40] = 0x02; /* GUID partition table */
	p8[41] = 0x02; /* GUID signature */
	/* Hard Drive Media Device Path -- END */
	/* File Path Media Device Path -- START */
	FilePathMediaDevicePath_Length = 4 + (strlen(args[LOADER_FILE_PATH])
								+ 1) * 2;
	buf_bytes_n += FilePathMediaDevicePath_Length;
	buf = realloc(buf, (size_t)buf_bytes_n);
	p8 = buf	+ sizeof(UEFIVarAttrs)
			+ sizeof(LoadOptionAttrs)
			+ sizeof(FilePathListLength)
			+ desc_bytes_n
			+ HardDriveMediaDevicePath_Length;
	p8[0] = 0x04; /* Media Device Path */
	p8[1] = 0x04; /* File Path */
	p16 = (u16*)(p8 + 2);
	*p16 = htole16(FilePathMediaDevicePath_Length);
	p8_src = args[LOADER_FILE_PATH];
	p8_dst = p8 + 4;
	loop { /* ascii to ucs2 */
		p8_dst[0] = p8_src[0];
		p8_dst[1] = 0;
		if (p8_src[0] == 0)
			break;
		++p8_src;
		p8_dst += 2;
	}
	/* File Path Media Device Path -- END */
	/* Device Path End Structure */
	buf_bytes_n += 4;
	buf = realloc(buf, (size_t)buf_bytes_n);
	p8 = buf	+ sizeof(UEFIVarAttrs)
			+ sizeof(LoadOptionAttrs)
			+ sizeof(FilePathListLength)
			+ desc_bytes_n
			+ HardDriveMediaDevicePath_Length
			+ FilePathMediaDevicePath_Length;
	p8[0] = 0x7f; /* End of Hardware Device Path */
	p8[1] = 0xff; /* End Entire Device Path */
	p8[2] = 0x04; /* u16 0x004, the bytes count of this structure */
	p8[3] = 0x00;
	/* we don't know FilePathListLength now */
	FilePathListLength = HardDriveMediaDevicePath_Length
					+ FilePathMediaDevicePath_Length + 4;
	p16 = (u16*)(buf	+ sizeof(UEFIVarAttrs)
				+ sizeof(LoadOptionAttrs));
	*p16 = htole16(FilePathListLength);
	/* loader arguments */
	if (args_n == 5) {
		buf_bytes_n += strlen(args[LOADER_ARGS]) * 2; /* no terminating 0 */
		buf = realloc(buf, (size_t)buf_bytes_n);
		p8_src = args[LOADER_ARGS];
		p8_dst = buf	+ sizeof(UEFIVarAttrs)
				+ sizeof(LoadOptionAttrs)
				+ sizeof(FilePathListLength)
				+ desc_bytes_n
				+ FilePathListLength;
		loop { /* ascii to ucs2 */
			if (p8_src[0] == 0)
				break;

			p8_dst[0] = p8_src[0];
			p8_dst[1] = 0;

			++p8_src;
			p8_dst += 2;
		}
	}
	written_bytes_n = write(1, buf, buf_bytes_n);
	if (written_bytes_n != buf_bytes_n) {
		fprintf(stderr, "ERROR:unable to write properly on the standard output the load option variable data\n");
		return 1;
	}
	return 0;
}
#undef loop
#undef strtoul
#undef u16
#undef u32
#undef u8
#undef u64

#undef EFI_VARIABLE_NON_VOLATILE
#undef EFI_VARIABLE_BOOTSERVICE_ACCESS
#undef EFI_VARIABLE_RUNTIME_ACCESS

#undef LOAD_OPTION_ACTIVE

#undef PARTITION_UUID
#undef PARTITION_NUMBER
#undef PARTITION_START_LBA
#undef PARTITION_LBAS_N
#undef DESCRIPTION
#undef LOADER_FILE_PATH
#undef LOADER_ARGS
